/*global SP*/

define(function (require, exports, module) {
	'use strict';

	var _getCurrentContext = require('Sharepoint');
	var Subscription = require('Subscription');
	var personTemplate = require('templates/Add-subscriber-form');
	var Promise = require('bluebird');

	var Person = {
		formListName: 'TC Sub Order Form - Attendee',
		save: function () {
			if (this.saveResult) {
				return this.saveResult;
			}
			var that = this;
			this.saveResult = new Promise(function (resolve, reject) {
				_getCurrentContext.then(function (clientContext) {
					/*jshint camelcase: false */
					var oList = clientContext.get_web().get_lists().getByTitle(that.formListName);
					var itemCreateInfo = new SP.ListItemCreationInformation();
					var oListItem = oList.addItem(itemCreateInfo);
					oListItem.set_item('First_x0020_Name', $('#' + that.domID + 'firstName').val());
					oListItem.set_item('Last_x0020_Name', $('#' + that.domID + 'lastName').val());
					oListItem.set_item('Title', $('#' + that.domID + 'email').val());
					oListItem.set_item('User_x0020_Type', $('#' + that.domID + 'userType option:selected').val());
					oListItem.set_item('Department', $('#' + that.domID + 'department').val());
					oListItem.set_item('Job_x0020_Role', $('#' + that.domID + 'jobRole').val());
					oListItem.set_item('FormID', that.formID);
					oListItem.update();
					clientContext.load(oListItem);
					var onSuccess = function () {
						resolve(oListItem);
					};
					var onFail = function (sender, args) {
						var error = new Error('failed to save person');
						error.sender = sender;
						error.args = args;
						reject(error);
					};
					clientContext.executeQueryAsync(Function.createDelegate(this, onSuccess),
						Function.createDelegate(this, onFail));
				});
			});
			return this.saveResult;
		},
		saveSubscriptions: function () {
			var that = this;
			return new Promise(function (resolve, reject) {
				that.save().then(function (item) {
					/*jshint camelcase: false */
					var personID = item.get_id();
					Promise.map(that.subscriptions, function (subscription) {
						subscription.personID = personID;
						return subscription.save();
					}).then(function (result) {
						resolve(result);
					}).caught(function (error) {
						reject(error);
					});
				}).caught(function (error) {
					reject(error);
				});
			});
		},
		saveAll: function () {
			return this.saveSubscriptions();
		},
		create: function () {
			var that = Object.create(Person);
			return that.init.apply(that, arguments);
		},
		init: function (domID, mountPoint, subscriptionOptions) {
			this.boundOnChange = this.onChange.bind(this);
			this.subscriptions = [];
			this.subscriptionOptions = subscriptionOptions;
			this.subCounter = 0;
			this.domID = domID;
			this.mountPoint = mountPoint;
			this.render();
			this.price = 0;
			this.keys = 0;
			this.formID = 0;
			this.index = 1;
			return this;
		},
		destroy: function () {
			this.boundOnChange = null;
			this.element.off();
			this.element.remove();
		},
		validate: function(){
			var valid = this.subscriptions.length > 0;
			this.element.find('.addSubscriptionBtn ~ p.error').toggle(!valid).attr('aria-hidden', valid);
			if (!valid) {
				this.element.find('.addSubscriptionBtn').focus();
			}
			return valid;
		},
		addSubscription: function () {
			var subID = this.domID + 'sub' + (this.subCounter++) + '_';
			var mountPoint = this.element.find('.subscriptions');
			var subscription = Subscription.create(subID, mountPoint, this.subscriptionOptions);
			this.subscriptions.push(subscription);
			this.checkRemovable();
			this.validate();
			return subscription;
		},
		removeSubscriptionByDomID: function (id) {
			var subscription = this.findSubscriptionByDomID(id);
			if (typeof subscription === 'undefined' || subscription === null) {
				throw new Error('No subscription with key ' + id);
			}
			subscription.destroy();
			this.subscriptions.splice(this.subscriptions.indexOf(subscription), 1);
			this.checkRemovable();
			this.element.trigger('selectChange');
		},
		checkRemovable: function () {
			var enable = this.subscriptions.length > 0;
			this.element.find('.removeAttendeeBtn').attr('disabled', enable).attr('aria-disabled', enable);
		},
		findSubscriptionByDomID: function (id) {
			var item;
			for (var i = 0, ii = this.subscriptions.length; i < ii; i++) {
				item = this.subscriptions[i];
				if (item.hasOwnProperty('domID') && item.domID === id) {
					return item;
				}
			}
			return null;
		},
		render: function () {
			var html = personTemplate({person: this});
			this.element = $(html);
			this.mountPoint.append(this.element);
			this.element.on('selectChange', this.boundOnChange);
			this.updateIndex();
			this.element.find(':input:first').focus();
		},
		updateIndex: function (index) {
			this.index = index;
			this.element.find('legend span.index').text(this.index);
		},
		onChange: function (event) {
			this.updateSubscriptionPriceOutput();
		},
		updateSubscriptionPriceOutput: function () {
			this.price = 0;
			this.keys = 0;
			var item;
			for (var i = 0, ii = this.subscriptions.length; i < ii; i++) {
				item = this.subscriptions[i];
				this.price += item.price;
				this.keys += item.keys;
			}
		}
	};
	return Person;

});