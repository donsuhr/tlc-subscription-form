/*global getQueryParameterByName */
'use strict';

require(['jquery'], function ($) {

	function go(TlcFormView) {
		var urlFormID = parseInt(getQueryParameterByName('formID'), 10);
		var view = TlcFormView.create($('#FormView'), urlFormID);
		view.showLoading();
		view.loadAll().then(function (result) {
			view.hideLoading();
			$('.printBtn').show();
		});
		$('.printBtn').on('click', function (event) {
            event.preventDefault();
			window.print();
		});
		var billType = getQueryParameterByName('billType');
		if (billType.indexOf('cc') !== -1){
			$('#ccDownload').show();
		}
	}

	if (typeof window.devmode !== 'undefined' && !window.requireTestMode) {
		console.log('load patch');
		require(['testLocal'], function () {
			require(['TlcFormView'], function (TlcFormView) {
				go(TlcFormView);
			});
		});
	} else {
		require(['TlcFormView'], function (TlcFormView) {
			go(TlcFormView);
		});
	}

});
