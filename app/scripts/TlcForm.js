/*global SP*/

define(function (require, exports, module) {

	'use strict';
	var Promise = require('bluebird');

	var $ = require('jquery');
	var Person = require('Person');
	var _getCurrentContext = require('Sharepoint');
	var SubscriptionOptions = require('SubscriptionOptions');
	var showHideLoading = require('showHideLoading');
	var template = require('templates/Tlc-subscribe-form');
	require('jquery.validate');

	var TlcForm = {
		formListName: 'TC Sub Order Form',
		save: function saveForm() {
			if (this.save.result) {
				return this.save.result;
			}
			var that = this;
			that.save.result = new Promise(function (resolve, reject) {
				_getCurrentContext.then(function (clientContext) {
					/*jshint camelcase: false */
					var oList = clientContext.get_web().get_lists().getByTitle(that.formListName);
					var itemCreateInfo = new SP.ListItemCreationInformation();
					var oListItem = oList.addItem(itemCreateInfo);
					oListItem.set_item('Title', $('#corpName').val());
					oListItem.set_item('Institution_x0020_Name', $('#institutionName').val());
					oListItem.set_item('PO', $('#poNumber').val());
					oListItem.set_item('Signature', $('#signature').val());
					oListItem.set_item('Title0', $('#title').val());
					oListItem.set_item('Payment_x0020_Option', $('input[name=paymentOption]:checked').val());
					oListItem.set_item('Agreed_x0020_to_x0020_Terms', $('#touAgree').val());
					oListItem.set_item('Numer_x0020_of_x0020_keys_x0020_', $('#useKeys').val());
					oListItem.set_item('Calculated_x0020_Total_x0020_Cos', that.totalPeoplePrice);

					oListItem.update();
					clientContext.load(oListItem);
					var onSuccess = function () {
						that.formID = oListItem.get_id();
						resolve(oListItem);
					};
					var onFail = function (sender, args) {
						var error = new Error('failed to save form');
						error.sender = sender;
						error.args = args;
						reject(error);
					};
					clientContext.executeQueryAsync(Function.createDelegate(this, onSuccess),
						Function.createDelegate(this, onFail));
				});
			});
			return that.save.result;
		},
		saveAttendees: function () {
			var that = this;
			return new Promise(function (resolve, reject) {
				that.save().then(function (item) {
					/*jshint camelcase: false */
					Promise.map(that.people, function (item) {
						item.formID = that.formID;
						return item.saveAll();
					}).then(function (result) {
						resolve(result);
					}).caught(function (error) {
						reject(error);
					});
				}).caught(function (error) {
					reject(error);
				});
			});
		},
		saveAll: function () {
			return this.saveAttendees();
		},
		create: function () {
			var proto = $.extend({}, showHideLoading, TlcForm);
			var instance = Object.create(proto);
			return TlcForm.init.apply(instance, arguments);
		},
		init: function (element) {
			this.totalPeopleKeys = 0;
			this.totalPeoplePrice = 0;
			this.alteredPrice = 0;
			this.boundOnChange = this.onChange.bind(this);
			this.personCounter = 0;
			this.people = [];
			this.element = element;
			this.formID = 0;
			this.subscriptionOptions = SubscriptionOptions.create();
			this.render();
			this.element.on('selectChange', this.boundOnChange);
			this.element.find('#useKeys').on('change keyup', this.boundOnChange);
			this.loadingAnimation = null;
			this.applyListeners();
			return this;
		},
		render: function(){
			var $html = $(template());
			this.element.append($html);
		},
		addPerson: function () {
			var domID = 'attendee' + (this.personCounter++) + '_';
			var mountPoint = this.element.find('#attendeeList');
			var person = Person.create(domID, mountPoint, this.subscriptionOptions);
			this.people.push(person);
			this.updatePeopleIndices();
			this.toggleError(!this.valid());
			return person;
		},
		removePersonByDomID: function (id) {
			var person = this.getPersonByDomID(id);
			if (typeof person === 'undefined' || person === null) {
				throw new Error('No person with key ' + id);
			}
			if (person.subscriptions.length) {
				throw new Error('Unable to remove people with subscriptions.');
			}
			this.people.splice(this.people.indexOf(person), 1);
			this.updatePeopleIndices();
			person.destroy();
			this.element.trigger('selectChange');
		},
		validate: function () {
			var childrenValid = this.people.every(function (element) {
				return element.validate();
			});
			var valid = this.valid();
			var showError = !valid && childrenValid;
			this.toggleError(showError);
			return valid && childrenValid;
		},
		valid: function () {
			return this.people.length > 0;
		},
		toggleError: function (show) {
			this.element.find('#addAttendeeBtn ~ p.error').toggle(show).attr('aria-hidden', !show);
			if (show) {
				this.element.find('#addAttendeeBtn').focus();
			}
			return this;
		},
		updatePeopleIndices: function () {
			var i, ii = this.people.length;
			for (i = 0; i < ii; i++) {
				this.people[i].updateIndex(i + 1);
			}
		},
		getPersonByDomID: function (id) {
			var item;
			for (var i = 0, ii = this.people.length; i < ii; i++) {
				item = this.people[i];
				if (item.hasOwnProperty('domID') && item.domID === id) {
					return item;
				}
			}
			return null;
		},
		onChange: function (event) {
			this.updateTotalPrice();
			this.updatePriceOutputDetails();
		},
		updateTotalPrice: function () {
			var item;
			this.totalPeopleKeys = 0;
			this.totalPeoplePrice = 0;
			var i, ii = this.people.length;
			for (i = 0; i < ii; i++) {
				item = this.people[i];
				this.totalPeoplePrice += item.price;
				this.totalPeopleKeys += item.keys;
			}
			var $useKeys = this.element.find('#useKeys');
			var currentUseKeyValue = parseInt($useKeys.val(), 10) || 0;
			var newVal = currentUseKeyValue;
			if (currentUseKeyValue !== 0) {
				newVal = Math.min(currentUseKeyValue, this.totalPeopleKeys);
				$useKeys.val(newVal);
			}
			this.alteredPrice = this.totalPeoplePrice - (newVal * 25);
		},
		updatePriceOutputDetails: function () {
			this.element.find('#subtotal').text('$' + this.totalPeoplePrice + ' (' + this.totalPeopleKeys + ' Keys)');
			this.element.find('#useKeys').attr('max', this.totalPeopleKeys);
			this.element.find('#alteredTotalPrice').text('$' + this.alteredPrice);
		},
		showLoading: function () {
			showHideLoading.showLoading.apply(this);
			var y = this.element.height() - this.loadingAnimation.height() * 2;
			this.loadingAnimation.css('top', y);
		},
		toggleSubmitBtn: function (enabled) {
			enabled = !!enabled;
			$('#submitBtn').prop('disabled', !enabled).attr('aria-disabled', !enabled);
		},
		showError: function (errorMsg) {
			this.element.find('#formErrors').text(errorMsg).show();
		},
		hideError: function () {
			this.element.find('#formErrors').hide();
		},
		applyListeners: function () {
			var boundAddAttendeeBtn = function (event) {
				event.preventDefault();
				this.addPerson();
			}.bind(this);
			this.element.find('#addAttendeeBtn').on('click', boundAddAttendeeBtn);
			var boundRemoveAttendeeBtn = function (event) {
				event.preventDefault();
				var key = $(event.target).attr('id').replace('rmPerson_', '');
				this.removePersonByDomID(key);
			}.bind(this);
			var boundAddSubscriptionBtn = function (event) {
				event.preventDefault();
				var domID = $(event.target).attr('id').replace('addSub_', '');
				var person = this.getPersonByDomID(domID);
				person.addSubscription();
			}.bind(this);
			var boundRemoveSubscriptionBtn = function (event) {
				event.preventDefault();
				var domID = $(event.target).attr('id').replace('rmSub_', '');
				var personDomID = domID.match(/attendee\d+_/)[0];
				var person = this.getPersonByDomID(personDomID);
				person.removeSubscriptionByDomID(domID);
			}.bind(this);
			this.element.find('#attendeeList')
				.on('click', '.removeAttendeeBtn', boundRemoveAttendeeBtn)
				.on('click', '.addSubscriptionBtn', boundAddSubscriptionBtn)
				.on('click', '.removeSubscriptionBtn', boundRemoveSubscriptionBtn);

			var boundSubmitHandler = function (form) {
				var that = this;
				if (!this.validate()) {
					return false;
				}
				this.showLoading();
				this.toggleSubmitBtn(false);
				this.hideError('');
				this.saveAll().then(function (result) {
					var paymentOption = that.element.find('input[name=paymentOption]:checked').val();
					var paymentOptionValue = paymentOption.toLowerCase().indexOf('send invoice') !== -1 ?
											 'invoice' :
											 'cc';
					var href = 'http://www.mycampusinsight.com/tlc/Pages/tlc-otc-subscription-form-complete.aspx';
					href += '?formID=' + that.formID;
					href += '&billType=' + paymentOptionValue;
					if (window.devmode) {
						console.log(href);
					} else {
						window.location.href = href;
					}
				}).caught(function (error) {
					that.toggleSubmitBtn(true);
					that.hideLoading();
					that.showError('Unable to save form. Please try again later.');
					console.log(error);
					if (error && error.hasOwnProperty('args') && 'get_message' in error.args) {
						/*jshint camelcase: false */
						console.log(error.args.get_message());
					}
					if (error.hasOwnProperty('stack')) {
						console.log(error.stack);
					}
				});
			}.bind(this);

			this.element.closest('form').validate({
				debug: true,
				submitHandler: boundSubmitHandler,
				showErrors: function (errorMap, errorList) {
					this.defaultShowErrors();
					$('label.error').each(function () {
						/*jshint sub:true*/
						var display = ($(this).prop('style')['display']);
						if (display === 'inline') {
							$(this).css('display', 'inline-block');
						}
					});

				}
			});
		}

	};

	return TlcForm;

});