'use strict';

require(['jquery'], function ($) {

	function go(TlcForm) {
		TlcForm.create($('#TlcForm'));
		//	var person = tlcForm.addPerson();
		//	person.addSubscription();
		// the sharepoint version over wrights String.prototype.trim w/o checking to see if it exists.
		// causes error in number validator in validation plugin
		$.origTrim = $.trim;
		$.trim = function (text) {
			text = text.toString();
			if (String.prototype.hasOwnProperty('trim')) {
				return String.prototype.trim.call(text);
			}
			return $.origTrim(text);
		};
	}

    if (typeof window.__gEditMode !== 'undefined' && window.__gEditMode) {
        console.log('edit mode, returning.');
        return;
    }

	if (typeof window.devmode !== 'undefined' && !window.requireTestMode) {
		console.log('load patch');
		require(['testLocal'], function () {
			require(['TlcForm'], function (TlcForm) {
				go(TlcForm);
			});
		});
	} else {
		require(['TlcForm'], function (TlcForm) {
			go(TlcForm);
		});
	}

});
