/* exported require */

'use strict';

/*jshint -W079 */
var require = {
	baseUrl: 'scripts',
	paths: {
		'jquery': '../../bower_components/jquery-legacy/jquery',
		'handlebars.runtime': '../../bower_components/handlebars/handlebars.runtime.amd',
		'jquery.validate': '../../bower_components/jquery-validation/dist/jquery.validate',
		'bluebird': '../../bower_components/bluebird/js/browser/bluebird',
		'jqueryTools': 'vendor/jquery.tools.1.2.6.min',
		'requireLib': '../../bower_components/requirejs/require',
		'templates': '../jst'
	},
	shim: {
		'jqueryTools': {
			deps: ['jquery']
		}
	},
	map: {
		'*': { 'jquery': 'jqueryFromGlobal' }
	}

};