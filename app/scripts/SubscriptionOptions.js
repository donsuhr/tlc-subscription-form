/*global SP*/

define(function (require, exports, module) {

	'use strict';

	var Promise = require('bluebird');
	var _getCurrentContext = require('Sharepoint');

	var SubscriptionOptions = {
		trainingLibraryListName: 'TrainingLibrary',
		load: function load() {
			if (this.loadResult) {
				return this.loadResult;
			}
			var that = this;
			this.loadResult = new Promise(function (resolve, reject) {
				_getCurrentContext.then(function (clientContext) {
					/*jshint camelcase: false */
					var oList = clientContext.get_web().get_lists().getByTitle(that.trainingLibraryListName);

					var query = new SP.CamlQuery();
					query.set_viewXml('<View>' +
							'<ViewFields><FieldRef Name="Title" /><FieldRef Name="ID" /><FieldRef Name="Cost" /><FieldRef Name="Keys" /></ViewFields>' +
							'<Query><Where><Eq><FieldRef Name="Show_x0020_on_x0020_Subscription"/><Value Type="Integer">1</Value></Eq></Where>' +
							'<OrderBy><FieldRef Name="Title"/></OrderBy>' +
							'</Query>' +
							'</View>'
					);
					var items = oList.getItems(query);
					clientContext.load(items);
					var onSuccess = function () {
						var listEnum = items.getEnumerator();
						var ret = [];
						var item;
						while (listEnum.moveNext()) {
							item = listEnum.get_current();
							ret.push({
								'title': $.trim(item.get_item('Title')) || 'Title not set.',
								'price': item.get_item('Cost'),
								'id': parseInt(item.get_item('ID'), 10),
								'keys': parseInt(item.get_item('Keys'), 10)
							});
						}
						that.isLoaded = true;
						that.options = ret;
						resolve(ret);
					};
					var onFail = function (sender, args) {
						var error = new Error('failed to get training library items');
						error.sender = sender;
						error.args = args;

						reject(sender, args);
					};
					clientContext.executeQueryAsync(Function.createDelegate(this, onSuccess),
						Function.createDelegate(this, onFail));

				});
			});
			return this.loadResult;
		},
		create: function () {
			return Object.create(this).init();
		},
		init: function () {
			this.isLoaded = false;
			this.options = [];
			return this;
		},
		getItemByID: function (id) {
			if (!this.isLoaded) {
				throw new Error('options not loaded');
			}
			var item;
			for (var i = 0, ii = this.options.length; i < ii; i++) {
				item = this.options[i];
				if (item.hasOwnProperty('id') && item.id === id) {
					return item;
				}
			}
			return {
				title: '',
				price: 0,
				keys: 0,
				id: 0
			};
		}
	};

	return SubscriptionOptions;

});