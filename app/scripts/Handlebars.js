define(['handlebars.runtime'], function (Handlebars) {
	'use strict';

	// Here you can register your Handlebars helpers like this:
	// Handlebars.default.registerHelper('foo', function () {
	//   return 'bar';
	// });
	//

	Handlebars.default.registerHelper('foo', function (value) {
		return '<span style="color:red">' + value + '</span>';
	});

	Handlebars.default.registerHelper('formatDate', function (value) {
		var date = new Date(value);
		return (date.getMonth()+1) + '/' + date.getDate() + '/' + date.getFullYear();
	});

	Handlebars.default.registerHelper('calcTotal', function (total, keys) {
		var keyValue = parseInt(keys, 10) * 25;
		return parseInt(total, 10) - keyValue;
	});

	return Handlebars.default;

	// for Handlebars 2.0.x you should return instead Handlebars.default;
});
