'use strict';

require(['jquery', 'jqueryTools'], function ($) {

	function go(TlcForm) {
		var tlcForm = TlcForm.create($('#TlcForm'));
		var person = tlcForm.addPerson();
		person.addSubscription();
	}

	if (typeof window.devmode !== 'undefined' && !window.requireTestMode) {
		console.log('load patch');
		require(['testLocal'], function () {
			require(['TlcForm'], function (TlcForm) {
				go(TlcForm);
			});
		});
	} else {
		require(['TlcForm'], function ($, TlcForm) {
			go(TlcForm);
		});
	}

});