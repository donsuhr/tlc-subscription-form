/* exported require */

'use strict';

/*jshint -W079 */
var require = {
	baseUrl: '/Style%20Library/mci/js/TlcForm/',
	paths: {
		'jquery': '/Style%20Library/vendor/jquery-legacy/jquery',
		'handlebars.runtime': '/Style%20Library/vendor/handlebars-1.3.0/handlebars.runtime.amd.min',
		'jquery.validate': '/Style%20Library/vendor/jquery-validation-1.13.0/dist/jquery.validate.min',
		'bluebird': '/Style%20Library/vendor/bluebird-2.3.0/bluebird',
		'templates': 'jst'
	},
	shim: {
		// jquery tools removed, using version already on the page via jqueryshim
		'jqueryTools': {
			deps: ['jquery']
		}
	},
	map: {
		'*': { 'jquery': 'jqueryFromGlobal' }
	},
	waitSeconds: 22
};
