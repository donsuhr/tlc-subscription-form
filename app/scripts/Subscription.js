/*global SP*/

define(function (require, exports, module) {

	'use strict';

	var _getCurrentContext = require('Sharepoint');
	var loadingTemplate = require('templates/Add-subscription-form--loading');
	var subscriptionTemplate = require('templates/Add-subscription-form');
	var Promise = require('bluebird');

	var Subscription = {
		formListName: 'TC Sub Order Form - Selected Course',
		save: function () {
			if (this.saveResult) {
				return this.saveResult;
			}
			var that = this;
			this.saveResult = new Promise(function (resolve, reject) {
				_getCurrentContext.then(function (clientContext) {
					/*jshint camelcase: false */
					var oList = clientContext.get_web().get_lists().getByTitle(that.formListName);
					var itemCreateInfo = new SP.ListItemCreationInformation();
					var oListItem = oList.addItem(itemCreateInfo);
					oListItem.set_item('Title', that.title);
					oListItem.set_item('Price_x0020_on_x0020_submit', that.price);
					oListItem.set_item('PersonID', that.personID);
					oListItem.set_item('CourseID', that.courseID);
					oListItem.update();
					clientContext.load(oListItem);
					var onSuccess = function () {
						resolve(oListItem);
					};
					var onFail = function (sender, args) {
						var error = new Error('failed to save subscription');
						error.sender = sender;
						error.args = args;
						reject(error);
					};
					clientContext.executeQueryAsync(Function.createDelegate(this, onSuccess),
						Function.createDelegate(this, onFail));
				});
			});
			return this.saveResult;
		},
		create: function () {
			var that = Object.create(Subscription);
			return that.init.apply(that, arguments);
		},
		init: function (domID, mountPoint, subscriptionOptions) {
			this.id = undefined;
			this.keys = 0;
			this.price = 0;
			this.title = '';
			this.domID = domID;
			this.personID = 0;
			this.courseID = 0;
			this.mountPoint = mountPoint;
			this.subscriptionOptions = subscriptionOptions;
			this.changeHandler = this.onChange.bind(this);
			this.render();
			return this;
		},
		render: function () {
			var html = loadingTemplate({subscription: this});
			this.element = $(html);
			this.mountPoint.append(this.element);
			var that = this;
			this.subscriptionOptions.load().then(function (results) {
				var html = subscriptionTemplate({subscription: that, subscriptions: results});
				var oldElement = that.element;
				that.element = $(html);
				oldElement.replaceWith(that.element);
				that.element.on('change', that.changeHandler);
				that.element.find('select').focus();
			});
		},
		destroy: function () {
			this.element.closest('fieldset').find('.addSubscriptionBtn').focus();
			this.element.off();
			this.element.remove();
		},
		onChange: function (event) {
			var $target = $(event.target);
			var value = parseInt($target.val(), 10);
			var info = this.subscriptionOptions.getItemByID(value);
			this.keys = info.keys;
			this.price = info.price;
			this.title = info.title;
			this.courseID = info.id;
			var isOptionSelected = info.id !== 0;
			this.element.find('p.label').toggleClass('is-hidden', !isOptionSelected);
			this.element.find('.feeAmount').text('Price: $' + info.price);
			this.element.find('.keys').text('Keys: ' + info.keys);
			this.element.find('.id').text(info.id);
			this.element.trigger('selectChange');
		}

	};

	return Subscription;

});