/*global ExecuteOrDelayUntilScriptLoaded, SP*/

define(function (require, exports, module) {

	'use strict';

	var Promise = require('bluebird');

	return (function _getCurrentContext() {
		var siteUrl = '/tlc/';
		return new Promise(function (resolve, reject) {
			var resolveAction = function () {
				resolve(new SP.ClientContext(siteUrl));
			};
			if (SP.ClientContext === undefined) {
				/*jshint newcap: false */
				ExecuteOrDelayUntilScriptLoaded(resolveAction, 'sp.js');
			} else {
				resolveAction();
			}
		});
	}());

});