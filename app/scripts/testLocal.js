define(function (require, exports, module) {

	'use strict';
	var Promise = require('bluebird');
	window.SP = {
		ClientContext: function () {
			/*jshint camelcase: false */
			return {
				get_web: function () {

					return {
						get_lists: function () {
							return {
								getByTitle: function () {
									return {
										getItems: function () {

										}
									};
								}
							};
						}
					};
				},
				load: function () {

				},
				executeQueryAsync: function () {

				}
			};
		},
		CamlQuery: function () {
			/*jshint camelcase: false */
			return {
				set_viewXml: function () {

				}
			};
		}
	};
	window.Function.createDelegate = function () {

	};
	window.getQueryParameterByName = function (item) {
		var obj = {
			formID: 22,
			billType: 'cc'
		};
		return obj[item];
	};

	require(['SubscriptionOptions'], function (SubscriptionOptions) {
		SubscriptionOptions.load = function () {
			if (SubscriptionOptions.load.result) {
				return SubscriptionOptions.load.result;
			}
			var that = this;
			SubscriptionOptions.load.result = new Promise(function (resolve, reject) {
				that.isLoaded = true;
				that.options =
					JSON.parse('[{"title":"Admissions Essentials","price":150,"id":50,"keys":6},' +
						'{"title":"Bursar Essentials","price":125,"id":6,"keys":5},' +
						'{"title":"Finance and Accounting Essentials","price":175,"id":7,"keys":7},' +
						'{"title":"Financial Aid Essentials","price":100,"id":51,"keys":4},' +
						'{"title":"User Account Management Essentials","price":200,"id":9,"keys":8}]');
				resolve(that.options);
			});
			return SubscriptionOptions.load.result;
		};
	});

	require(['TlcForm'], function (TlcForm) {
		TlcForm.save = function () {
			if (this.save.result) {
				return this.save.result;
			}
			var that = this;
			this.save.result = new Promise(function (resolve, reject) {
				setTimeout(function () {
					/*jshint camelcase: false */
					var id = Math.floor(Math.random() * 22) + 1;
					var fakeResult = {
						get_id: function () {
							return id;
						}
					};
					that.id = id;
					resolve(fakeResult);
				}, 500);
			});
			return this.save.result;
		};
	});

	require(['Person'], function (Person) {
		Person.save = function () {
			if (this.saveResult) {
				return this.save.result;
			}
			this.saveResult = new Promise(function (resolve, reject) {
				setTimeout(function () {
					/*jshint camelcase: false */
					var id = Math.floor(Math.random() * 22) + 1;
					var fakeResult = {
						get_id: function () {
							return id;
						}
					};
					resolve(fakeResult);
				}, 250);
			});
			return this.saveResult;
		};
		Person.email = 'x@y.com';
		Person.firstName = 'fn';
		Person.lastName = 'ln';
	});

	require(['Subscription'], function (Subscription) {
		Subscription.save = function () {
			if (this.saveResult) {
				return this.save.result;
			}
			this.saveResult = new Promise(function (resolve, reject) {
				setTimeout(function () {
					/*jshint camelcase: false */
					var id = Math.floor(Math.random() * 22) + 1;
					var fakeResult = {
						get_id: function () {
							return id;
						}
					};
					resolve(fakeResult);
				}, 225);
			});
			return this.saveResult;
		};
	});
	require(['TlcFormView'], function (TlcFormView) {
		TlcFormView.load = function () {
			if (TlcFormView.load.result) {
				return TlcFormView.load.result;
			}
			var that = this;
			TlcFormView.load.result = new Promise(function (resolve, reject) {
				that.isLoaded = true;
				that.data =
					JSON.parse('[{"id":22,"title":"Campus","institutionName":"Acme","po":"1234","signature":"lenny","jobTitle":"admin","paymentOption":"Bill CC","agreedTos":true,"useKeys":3,"calculatedTotalCost":125,"created":"2014-08-20T15:55:27.000Z"}]');
				setTimeout(function () {
					resolve(that.data);
				}, 1);
			});
			return TlcFormView.load.result;
		};

		TlcFormView.loadSubscribers = function () {
			var that = this;
			return new Promise(function (resolve, reject) {
				that.peopleData =
					JSON.parse('[{"id":7,"email":"lschloss@campusmgmt.com","firstName":"lenny","lastName":"schloss","userType":"Administrator","department":"x","jobRole":"ccc"},{"id":8,"email":"lschloss2@campusmgmt.com","firstName":"lenny","lastName":"schloss","userType":"Administrator","department":"x","jobRole":"ccc"}]');
				setTimeout(function () {
					resolve(that.peopleData);
				}, 1);
			});
		};

		TlcFormView.loadSubscriptions = function (subscriberID) {
			return new Promise(function (resolve, reject) {
				var ret = JSON.parse('[{"id":8,"title":"Finance and Accounting Essentials","price":125,"course":{"id":7,"title":"Finance and Accounting Essentials"},"person":{"id":7,"name":"7"}},{"id":8,"title":"Finance and Accounting Essentials","price":125,"course":{"id":7,"title":"Finance and Accounting Essentials"},"person":{"id":7,"name":"7"}}]');
				setTimeout(function () {
					console.log('done');
					resolve(ret);
				}, 1);
			});
		};

	});
});