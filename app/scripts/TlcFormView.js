/*global SP*/
/*jshint unused: false */

define(function (require, exports, module) {

	'use strict';
	var Promise = require('bluebird');
	var $ = require('jquery');
	var _getCurrentContext = require('Sharepoint');

	var showHideLoading = require('showHideLoading');

	var template = require('templates/formView');
	var templateCx = require('templates/formView-cx');
	var peopleTemplate = require('templates/personView');
	var peopleTemplateCx = require('templates/personView-cx');
	var subscriptionTemplate = require('templates/subscriptionView');
	var subscriptionTemplateCx = require('templates/subscriptionView-cx');

	var TlcFormView = {
		create: function () {
			var proto = $.extend({}, showHideLoading, TlcFormView);
			var instance = Object.create(proto);
			return TlcFormView.init.apply(instance, arguments);
		},
		init: function (element, id) {
			this.element = element;
			this.formID = id;
			this.data = [];
			this.viewType = this.element.attr('data-view') || 'cx';
			return this;
		},
		formListName: 'TC Sub Order Form',
		personListName: 'TC Sub Order Form - Attendee',
		subscriptionListName: 'TC Sub Order Form - Selected Course',
		load: function load() {
			if (this.loadResult) {
				return this.loadResult;
			}
			var that = this;
			this.loadResult = new Promise(function (resolve, reject) {
				_getCurrentContext.then(function (clientContext) {
					/*jshint camelcase: false */
					var oList = clientContext.get_web().get_lists().getByTitle(that.formListName);

					var query = new SP.CamlQuery();
					query.set_viewXml('<View>' +
							'<ViewFields><FieldRef Name="Title" /><FieldRef Name="ID" /><FieldRef Name="Institution_x0020_Name" /><FieldRef Name="PO" />' +
							'<FieldRef Name="Signature" /><FieldRef Name="Title0" /><FieldRef Name="Payment_x0020_Option" />' +
							'<FieldRef Name="Agreed_x0020_to_x0020_Terms" /><FieldRef Name="Numer_x0020_of_x0020_keys_x0020_" /><FieldRef Name="Calculated_x0020_Total_x0020_Cos" />' +
							'</ViewFields>' +
							'<Query><Where><Eq><FieldRef Name="ID"/><Value Type="Integer">' + that.formID +
							'</Value></Eq></Where>' +
							'<OrderBy><FieldRef Name="Title"/></OrderBy>' +
							'</Query>' +
							'</View>'
					);

					var items = oList.getItems(query);
					clientContext.load(items);
					var onSuccess = function () {
						var listEnum = items.getEnumerator();
						var ret = [];
						var item;
						while (listEnum.moveNext()) {
							item = listEnum.get_current();
							ret.push({
								'id': item.get_item('ID'),
								'title': $.trim(item.get_item('Title')) || 'Title not set.',
								'institutionName': item.get_item('Institution_x0020_Name'),
								'po': item.get_item('PO'),
								'signature': item.get_item('Signature'),
								'jobTitle': item.get_item('Title0'),
								'paymentOption': item.get_item('Payment_x0020_Option'),
								'agreedTos': item.get_item('Agreed_x0020_to_x0020_Terms'),
								'useKeys': item.get_item('Numer_x0020_of_x0020_keys_x0020_'),
								'calculatedTotalCost': item.get_item('Calculated_x0020_Total_x0020_Cos'),
								'created': item.get_item('Created')
							});
						}
						that.isLoaded = true;
						that.data = ret;
						resolve(ret);
					};
					var onFail = function (sender, args) {
						var error = new Error('failed to get training library items');
						error.sender = sender;
						error.args = args;

						reject(sender, args);
					};
					clientContext.executeQueryAsync(Function.createDelegate(this, onSuccess),
						Function.createDelegate(this, onFail));

				});
			});
			return this.loadResult;
		},
		loadSubscribers: function () {
			var that = this;
			return new Promise(function (resolve, reject) {
				_getCurrentContext.then(function (clientContext) {
					/*jshint camelcase: false */
					var oList = clientContext.get_web().get_lists().getByTitle(that.personListName);

					var query = new SP.CamlQuery();
					query.set_viewXml('<View>' +
							'<ViewFields><FieldRef Name="Title" /><FieldRef Name="ID" /><FieldRef Name="First_x0020_Name" /><FieldRef Name="Last_x0020_Name" />' +
							'<FieldRef Name="User_x0020_Type" /><FieldRef Name="Department" /><FieldRef Name="Job_x0020_Role" />' +
							'</ViewFields>' +
							'<Query><Where><Eq><FieldRef Name="FormID"/><Value Type="Integer">' + that.formID +
							'</Value></Eq></Where>' +
							'<OrderBy><FieldRef Name="Title"/></OrderBy>' +
							'</Query>' +
							'</View>'
					);

					var items = oList.getItems(query);
					clientContext.load(items);
					var onSuccess = function () {
						var listEnum = items.getEnumerator();
						var ret = [];
						var item;
						while (listEnum.moveNext()) {
							item = listEnum.get_current();
							ret.push({
								'id': item.get_item('ID'),
								'email': $.trim(item.get_item('Title')),
								'firstName': item.get_item('First_x0020_Name'),
								'lastName': item.get_item('Last_x0020_Name'),
								'userType': item.get_item('User_x0020_Type'),
								'department': item.get_item('Department'),
								'jobRole': item.get_item('Job_x0020_Role')
							});
						}
						that.peopleData = ret;
						resolve(ret);
					};
					var onFail = function (sender, args) {
						var error = new Error('failed to get subscriber library items');
						error.sender = sender;
						error.args = args;

						reject(sender, args);
					};
					clientContext.executeQueryAsync(Function.createDelegate(this, onSuccess),
						Function.createDelegate(this, onFail));

				});
			});
		},

		loadSubscriptions: function (subscriberID) {
			var that = this;
			return new Promise(function (resolve, reject) {
				_getCurrentContext.then(function (clientContext) {
					/*jshint camelcase: false */
					var oList = clientContext.get_web().get_lists().getByTitle(that.subscriptionListName);

					var query = new SP.CamlQuery();
					query.set_viewXml('<View>' +
							'<ViewFields><FieldRef Name="Title" /><FieldRef Name="ID" /><FieldRef Name="Price_x0020_on_x0020_submit" /><FieldRef Name="PersonID" />' +
							'<FieldRef Name="CourseID" /><FieldRef Name="CourseID_x003a_Course_x0020_Name" />' +
							'</ViewFields>' +
							'<Query><Where><Eq><FieldRef Name="PersonID"/><Value Type="Integer">' + subscriberID +
							'</Value></Eq></Where>' +
							'<OrderBy><FieldRef Name="Title"/></OrderBy>' +
							'</Query>' +
							'</View>'
					);

					var items = oList.getItems(query);
					clientContext.load(items);
					var onSuccess = function () {
						var listEnum = items.getEnumerator();
						var ret = [];
						var item, person, course;
						while (listEnum.moveNext()) {
							item = listEnum.get_current();
							person = item.get_item('PersonID');
							course = item.get_item('CourseID_x003a_Course_x0020_Name');
							ret.push({
								'id': item.get_item('ID'),
								'title': $.trim(item.get_item('Title')),
								'price': item.get_item('Price_x0020_on_x0020_submit'),
								'course': {id: course.get_lookupId(), title: course.get_lookupValue()},
								'person': {id: person.get_lookupId()}
							});
						}
						resolve(ret);
					};
					var onFail = function (sender, args) {
						var error = new Error('failed to get subscriber library items');
						error.sender = sender;
						error.args = args;

						reject(sender, args);
					};
					clientContext.executeQueryAsync(Function.createDelegate(this, onSuccess),
						Function.createDelegate(this, onFail));

				});
			});
		},

		renderSubscribers: function () {
			var template = this.viewType === 'cx' ? peopleTemplateCx : peopleTemplate;
			var html = template({items: this.peopleData});
			this.element.find('.subscribers').html(html);
		},
		renderSubscription: function (resultMap) {
			var html, id;
			var that = this;
			resultMap.map(function (item) {
				if (item.length) {
					var template = that.viewType === 'cx' ? subscriptionTemplateCx : subscriptionTemplate;
					html = template({items: item});
					id = item[0].person.id;
					that.element.find('#person_' + id + ' .training').append(html);
				}
			});
		},

		loadAll: function () {
			var that = this;

			return that.load().then(function (result) {
				that.render();
				return that.loadSubscribers();
			}).then(function (result) {
				that.renderSubscribers();
				return Promise.map(result, function (subscriber) {
					return that.loadSubscriptions(subscriber.id);
				});
			}).then(function (result) {
				that.renderSubscription(result);
			});
		},
		render: function () {
			var hasLoading = !!this.loadingAnimation;
			var data = this.data[0];
			var formTemplate = this.viewType === 'cx' ? templateCx : template;
			var html = formTemplate(data);
			this.element.html(html);
			if (hasLoading) {
				this.showLoading();
			}

		}
	};

	return TlcFormView;

});