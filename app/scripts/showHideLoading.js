define(function (require, exports, module) {

	'use strict';

	var $ = require('jquery');

	return {
		loadingAnimation: null,
		showLoading: function () {
			var exists = typeof this.loadingAnimation !== 'undefined' && this.loadingAnimation !== null;
			var detached = (exists && !$.contains(document, this.loadingAnimation[0]));
			if (!exists || detached) {
				this.loadingAnimation = $('<div class="loading">Loading...</div>');
				this.element.append(this.loadingAnimation);
			}
			var x = this.element.width() / 2 - this.loadingAnimation.width() / 2;
			var y = this.element.height() / 2 - this.loadingAnimation.height() / 2;
			this.loadingAnimation.css({left: x, top: y});
		},
		hideLoading: function () {
			if (this.loadingAnimation) {
				this.loadingAnimation.remove();
				this.loadingAnimation = null;
			}
		}
	};

});