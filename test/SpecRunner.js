require.config({
	baseUrl: '../app/scripts',
	paths: {
		'jquery': '../../bower_components/jquery/dist/jquery',
		'fixtures': '/test/bower_components/fixtures/fixtures',
		'sinon': '/test/bower_components/sinonjs/sinon',
		'es5-shim': '../../bower_components/es5-shim/es5-shim',
		'es5-sham': '../../bower_components/es5-shim/es5-sham',
		'bluebird': '../../bower_components/bluebird/js/browser/bluebird',
		'handlebars.runtime': '../../bower_components/handlebars/handlebars.runtime.amd',
		'spec': '../../test/spec',
		'templates': '../jst',
		'jquery.validate': '../../bower_components/jquery-validation/dist/jquery.validate'
	},
	deps: ['es5-shim', 'es5-sham', 'testLocal']
});

require(['require', 'jquery'], function (require) {
	/*globals mocha */
	'use strict';
	require([
		'spec/test',
		'spec/SubscriptionSpec',
		'spec/TlcFormSpec',
		'spec/PersonSpec'
	], function (require) {
		mocha.run();
	});

});