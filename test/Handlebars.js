define(['handlebars.runtime'], function (Handlebars) {
	'use strict';

	// Here you can register your Handlebars helpers like this:
	// Handlebars.default.registerHelper('foo', function () {
	//   return 'bar';
	// });
	//

	Handlebars.default.registerHelper('foo', function (value) {
		return '<span style="color:red">' + value + '</span>';
	});


	return Handlebars.default;

	// for Handlebars 2.0.x you should return instead Handlebars.default;
});