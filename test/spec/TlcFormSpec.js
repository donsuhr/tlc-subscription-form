define(function (require) {
	'use strict';

	//https://github.com/badunk/js-fixtures

	var TlcForm = require('TlcForm');

	var form, person, $doc;

	describe('TlcForm', function () {
		beforeEach(function () {
			//fixtures.set(loadingTemplate + subscriptionTemplate + personTemplate + mountPoint);
			fixtures.load('Subscription.html');
			form = TlcForm.create($(fixtures.window().document).find('#sut'));
			$doc = $(fixtures.window().document).find('#sut');
		});
		afterEach(function () {
			fixtures.cleanUp(); // cleans up the fixture for the next test
		});

		it('should run here few assertions', function () {
			expect(true).to.equal(true);
		});

		it('should have people', function () {
			expect(form.people.length).to.equal(0);
			var person = form.addPerson();
			expect(form.people.length).to.equal(1);
			expect(form.people[0]).to.equal(person);
		});

		it('should remove people', function () {
			var key = form.addPerson().domID;
			expect(form.people.length).to.equal(1);
			form.removePersonByDomID(key);
			expect(form.people.length).to.equal(0);
		});

		it('should not remove people with subscriptions', function () {
			var person = form.addPerson();
			person.addSubscription();
			var fn = function () {
				form.removePersonByKey(0);
			};
			expect(fn).to.throw(Error);
			expect(form.people.length).to.equal(1);
		});

		it('should exception on remove unknown key', function () {
			var fn = function () {
				form.removePersonByKey(222);
			};
			expect(fn).to.throw(Error);
		});

		it('should return a person by key', function () {
			var person = form.addPerson();
			var key = person.domID;
			var returnedPerson = form.getPersonByDomID(key);
			expect(returnedPerson).to.equal(person);
		});

		describe('TlcForm Person Subscription', function () {

			beforeEach(function () {
				person = form.addPerson();
			});

			it('should add subscriptions', function () {
				/* jshint expr:true */
				var subscription = person.addSubscription();
				expect(subscription).not.to.be.undefined;
				expect(subscription.id).to.be.undefined;
			});

			it('should remove subscriptions', function () {
				var subscription = person.addSubscription();
				var key = subscription.domID;
				expect(person.subscriptions.length).to.equal(1);
				person.removeSubscriptionByDomID(key);
				expect(person.subscriptions.length).to.equal(0);
			});
		});

		describe('TlcForm can validate', function () {
			/* jshint expr:true */
			it('is not valid with no people', function(){
				expect(form.validate()).to.be.false;
			});

			it('is valid with one person', function(){
				var person = form.addPerson();
				// person requires subscription to be valid
				person.addSubscription();
				expect(form.validate()).to.be.true;
			});

			it ('is invalid with one invalid person', function(){
				form.addPerson();
				expect(form.validate()).to.be.false;
			});

			it('should show an error when invalid', function(){
				expect($doc.find('#attendeeList ~ p.error:visible').length).to.equal(0);
				form.validate();
				expect($doc.find('#attendeeList ~ p.error:visible').length).to.equal(1);
			});
		});
	});
});
