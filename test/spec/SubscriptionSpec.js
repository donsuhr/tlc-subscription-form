define(function (require) {
	'use strict';

	var mountPoint = '<div id="sut"></div>';
	var sut;

	var $ = require('jquery');
	var Subscription = require('Subscription');

	var SubscriptionOptions = require('SubscriptionOptions');
	var subscriptionOptions = SubscriptionOptions.create();

	describe('Subscription', function () {
		beforeEach(function () {
			fixtures.set(mountPoint);
			sut = Subscription.create('sutID', $(fixtures.window().document).find('#sut'), subscriptionOptions);
		});
		afterEach(function () {
			fixtures.cleanUp(); // cleans up the fixture for the next test
		});

		it('should run here few assertions', function () {
			expect(true).to.equal(true);
		});

	});
});
