define(function (require) {
	'use strict';

	var $ = require('jquery');
	var Person = require('Person');

	var SubscriptionOptions = require('SubscriptionOptions');
	var subscriptionOptions = SubscriptionOptions.create();

	var person, $doc;

	describe('Person', function () {
		beforeEach(function () {
			fixtures.load('Person.html');
			person =
				Person.create('sutID', $(fixtures.window().document).find('#sut #attendeeList'), subscriptionOptions);
			$doc = $(fixtures.window().document).find('#sut');
		});
		afterEach(function () {
			fixtures.cleanUp(); // cleans up the fixture for the next test
		});

		it('should run here few assertions', function () {
			expect(true).to.equal(true);
		});

		it('should create a person', function () {
			/* jshint expr:true */
			expect(person).to.not.be.undefined;
		});

		describe('Person can validate', function () {
			/* jshint expr:true */
			it('should require one subscription before being valid', function () {
				expect(person.validate()).to.be.false;
			});
			it('is valid with one subscription', function () {
				person.addSubscription();
				expect(person.validate()).to.be.true;
			});

			it('shows an error when invalid', function () {
				expect($doc.find('.subscriptions ~ p.error:visible').length).to.equal(0);
				person.validate();
				expect($doc.find('.subscriptions ~ p.error:visible').length).to.equal(1);
			});
		});

	});
});
